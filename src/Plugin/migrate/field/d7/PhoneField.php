<?php

namespace Drupal\whatsapp\Plugin\migrate\field\d7;

use Drupal\migrate_drupal\Plugin\migrate\field\FieldPluginBase;

/**
 * @MigrateField(
 *   id = "phone",
 *   type_map = {
 *     "phone" = "whatsapp",
 *   },
 *   core = {7},
 *   source_module = "phone",
 *   destination_module = "whatsapp"
 * )
 */
class PhoneField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'whatsapp' => 'basic_string',
    ];
  }

}
