<?php

namespace Drupal\Tests\whatsapp\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the creation of telephone fields.
 *
 * @group whatsapp
 */
class WhatsappFieldTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'field',
    'node',
    'whatsapp',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to create articles.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article']);
    $this->webUser = $this->drupalCreateUser(['create article content', 'edit own article content']);
    $this->drupalLogin($this->webUser);

    // Add the whatsapp field to the article content type.
    FieldStorageConfig::create([
      'field_name' => 'field_whatsapp',
      'entity_type' => 'node',
      'type' => 'whatsapp',
    ])->save();
    FieldConfig::create([
      'field_name' => 'field_whatsapp',
      'label' => 'WhatsApp Number',
      'entity_type' => 'node',
      'bundle' => 'article',
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', 'article')
      ->setComponent('field_whatsapp', [
        'type' => 'whatsapp_default',
        'settings' => [
          'placeholder' => '123-456-7890',
        ],
      ])
      ->save();

    $display_repository->getViewDisplay('node', 'article')
      ->setComponent('field_whatsapp', [
        'type' => 'whatsapp_link',
        'weight' => 1,
      ])
      ->save();
  }

  /**
   * Test to confirm the widget is setup.
   *
   * @covers \Drupal\whatsapp\Plugin\Field\FieldWidget\WhatsappDefaultWidget::formElement
   */
  public function testWhatsappWidget() {
    $this->drupalGet('node/add/article');
    $this->assertFieldByName("field_whatsapp[0][value]", '', 'Widget found.');
    $this->assertRaw('placeholder="123-456-7890"');
  }

  /**
   * Test the whatsapp formatter.
   *
   * @covers \Drupal\whatsapp\Plugin\Field\FieldFormatter\WhatsappLinkFormatter::viewElements
   *
   * @dataProvider providerPhoneNumbers
   */
  public function testWhatsappFormatter($input, $expected) {
    // Test basic entry of whatsapp field.
    $edit = [
      'title[0][value]' => $this->randomMachineName(),
      'field_whatsapp[0][value]' => $input,
    ];

    $this->drupalPostForm('node/add/article', $edit, t('Save'));
    $this->assertRaw('<a href="whatsapp:' . $expected . '">');
  }

  /**
   * Provides the phone numbers to check and expected results.
   */
  public function providerPhoneNumbers() {
    return [
      'standard phone number' => ['123456789', '123456789'],
      'whitespace is removed' => ['1234 56789', '123456789'],
      'parse_url(0) return FALSE workaround' => ['0', '0-'],
      'php bug 70588 workaround - lower edge check' => ['1', '1-'],
      'php bug 70588 workaround' => ['123', '1-23'],
      'php bug 70588 workaround - with whitespace removal' => ['1 2 3 4 5', '1-2345'],
      'php bug 70588 workaround - upper edge check' => ['65534', '6-5534'],
      'php bug 70588 workaround - edge check' => ['65535', '6-5535'],
      'php bug 70588 workaround - invalid port number - lower edge check' => ['65536', '6-5536'],
      'php bug 70588 workaround - invalid port number - upper edge check' => ['99999', '9-9999'],
      'lowest number not affected by php bug 70588' => ['100000', '100000'],
    ];
  }

}
