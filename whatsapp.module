<?php

/**
 * @file
 * Defines a simple whatsapp number field type.
 */

use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function whatsapp_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.whatsapp':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The WhatsApp module allows you to create fields that contain whatsapp numbers. See the <a href=":field">Field module help</a> and the <a href=":field_ui">Field UI help</a> pages for general information on fields and how to create and manage them. For more information, see the <a href=":telephone_documentation">online documentation for the WhatsApp module</a>.', [':field' => Url::fromRoute('help.page', ['name' => 'field'])->toString(), ':field_ui' => (\Drupal::moduleHandler()->moduleExists('field_ui')) ? Url::fromRoute('help.page', ['name' => 'field_ui'])->toString() : '#', ':whatsapp_documentation' => 'https://www.drupal.org/documentation/modules/whatsapp']) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Managing and displaying WhatsApp fields') . '</dt>';
      $output .= '<dd>' . t('The <em>settings</em> and the <em>display</em> of the WhatsApp field can be configured separately. See the <a href=":field_ui">Field UI help</a> for more information on how to manage fields and their display.', [':field_ui' => (\Drupal::moduleHandler()->moduleExists('field_ui')) ? Url::fromRoute('help.page', ['name' => 'field_ui'])->toString() : '#']) . '</dd>';
      $output .= '<dt>' . t('Displaying WhatsApp numbers as links') . '</dt>';
      $output .= '<dd>' . t('WhatsApp numbers can be displayed as links with the scheme name <em>whatsapp:</em> by choosing the <em>WhatsApp</em> display format on the <em>Manage display</em> page. Any spaces will be stripped out of the link text. This semantic markup improves the user experience on mobile and assistive technology devices.') . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function whatsapp_field_formatter_info_alter(&$info) {
  $info['string']['field_types'][] = 'whatsapp';
}
